//
//  LocalGameViewController.swift
//  TicTacToe
//
//  Created by Kiri on 29/11/2019.
//  Copyright © 2019 Kiri. All rights reserved.
//

import UIKit

class LocalGameViewController: BaseTicTacToeViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerSymbol = .X
    }
    
    override func cellClickedAction(_ index: Int) {
        self.items[index].value = playerSymbol
        if let index = self.selectedItemIndex {
            self.items[index].value = .E
            self.selectedItemIndex = nil
        }
        self.collectionView.reloadData()
        self.checkWinner(self.playerSymbol)
        if self.playerSymbol == .O {
            self.playerSymbol = .X
        } else {
            self.playerSymbol = .O
        }
    }
}
