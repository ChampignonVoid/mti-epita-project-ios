//
//  FindBluetoothViewController.swift
//  TicTacToe
//
//  Created by Kiri on 24/11/2019.
//  Copyright © 2019 Kiri. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class MultipeerViewController: BaseTicTacToeViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var peerID: MCPeerID!
    private var mcSession: MCSession!
    private var mcAdvertiserAssistant: MCAdvertiserAssistant!
    
    private var isPlayerTurn: Bool!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        peerID = MCPeerID(displayName: UIDevice.current.name)
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession.delegate = self
        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: "tic-tae-toe", discoveryInfo: nil, session: mcSession)
        isPlayerTurn = false
        
        let ac = UIAlertController(title: "Connection menu", message: nil, preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title: "Héberger une session", style: .default, handler: startHosting))
        ac.addAction(UIAlertAction(title: "Rejoindre une session", style: .default, handler: joinSession))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            self.navigationController?.popViewController(animated: true)
        }))
        present(ac, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        mcAdvertiserAssistant?.stop()
        mcSession?.disconnect()
    }
    
    override func cellClickedAction(_ index: Int) {
        sendData(index)
    }
    
    private func sendData(_ index: Int) {
        let oldIndex = self.selectedItemIndex ?? -1
        let dataModel = MultipeerTicTacToeData(oldIndex: oldIndex, newIndex: index, symbol: playerSymbol)
        do {
            let jsonData = try JSONEncoder().encode(dataModel)
            try mcSession.send(jsonData, toPeers: mcSession.connectedPeers, with: .reliable)
            isPlayerTurn = false
            items[index].value = playerSymbol
            if let index = self.selectedItemIndex {
                self.items[index].value = .E
                self.selectedItemIndex = nil
            }
            collectionView.reloadData()
            checkWinner(self.playerSymbol)
        }
        catch {
            print("Error while sending message/parsing data")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.isPlayerTurn {
            super.collectionView(collectionView, didSelectItemAt: indexPath)
        }
    }
}

/// Multipeer extension
extension MultipeerViewController: MCSessionDelegate, MCBrowserViewControllerDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected")
        case .connecting:
            print("Connecting...")
        case .notConnected:
            print("Not connected")
        @unknown default:
            fatalError()
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async {
            [unowned self] in
            do {
                let dataModel = try JSONDecoder().decode(MultipeerTicTacToeData.self, from: data)
                self.items[dataModel.newIndex].value = dataModel.symbol
                if (dataModel.oldIndex >= 0) {
                    self.items[dataModel.oldIndex].value = .E
                }
                self.isPlayerTurn = true
                self.collectionView.reloadData()
                self.checkWinner(dataModel.symbol)
            }
            catch {
                print("Error with the JSONDecoder")
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func startHosting(action: UIAlertAction) {
        mcAdvertiserAssistant.start()
        playerSymbol = .X
        isPlayerTurn = true
    }
    
    func joinSession(action: UIAlertAction) {
        let mcBrowser = MCBrowserViewController(serviceType: "tic-tae-toe", session: mcSession)
        mcBrowser.delegate = self
        playerSymbol = .O
        isPlayerTurn = false
        self.present(mcBrowser, animated: true)
    }
}
