//
//  TicTacToeViewCellCollectionViewCell.swift
//  TicTacToe
//
//  Created by Kiri on 24/11/2019.
//  Copyright © 2019 Kiri. All rights reserved.
//

import UIKit

class TicTacToeViewCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentLabel: UILabel!
}
