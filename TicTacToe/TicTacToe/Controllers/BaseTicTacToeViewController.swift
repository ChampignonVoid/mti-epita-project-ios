//
//  BaseTicTacToeViewController.swift
//  TicTacToe
//
//  Created by Kiri on 29/11/2019.
//  Copyright © 2019 Kiri. All rights reserved.
//

import UIKit

class BaseTicTacToeViewController: UIViewController {
    var items: [TicTacToeCellModel]!
    var playerSymbol = CellType.E
    var selectedItemIndex: Int?
    
    static let rowSize = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()

        items = [TicTacToeCellModel]()
        
        for _ in 0..<(BaseTicTacToeViewController.rowSize * BaseTicTacToeViewController.rowSize) {
            items.append(TicTacToeCellModel())
        }
    }
    
    func checkWinner(_ playerSymbol: CellType) {
        // TODO: Add algorithm
        var winner = false
        let winningSequences: [Set<Int>] = [
            // Horizontal
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            // Diagonal
            [0, 4, 8],
            [2, 4, 6],
            // Vertical
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8]
        ]
        
        var playerSymbolIndexes = [Int]()
        for i in 0..<self.items!.count {
            if self.items[i].value == playerSymbol {
                playerSymbolIndexes.append(i)
            }
        }
        for seq in winningSequences {
            if seq.isSubset(of: playerSymbolIndexes) {
                winner = true
                break
            }
        }
        if winner {
            let title = "\(playerSymbol) a gagné :) !"
            let ac = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Revenir au menu.", style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            }))
            present(ac, animated: true)
        }
    }
    
    // To be implemented on subclasses
    func cellClickedAction(_ index: Int) {
        abort()
    }
}

extension BaseTicTacToeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TicTacToeViewCellCollectionViewCell
        cell.contentLabel.text = items[indexPath.item].value.rawValue
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.black.cgColor
        if let index = selectedItemIndex,
            index == indexPath.item {
            cell.layer.backgroundColor = UIColor.cyan.cgColor
        }
        else if isFreeAdjacent(indexPath.item) {
            cell.layer.backgroundColor = UIColor.yellow.cgColor
        }
        else {
            cell.layer.backgroundColor = UIColor.white.cgColor
        }
        return cell
    }
    
    func isFreeAdjacent(_ indexPathItem: Int) -> Bool {
        guard let index = selectedItemIndex else {
            return false
        }
        
        // Calculate all neighbours index
        var neighboursIndex = [index - MultipeerViewController.rowSize, index + MultipeerViewController.rowSize]
        
        // Horizontal row check
        if ((index - 1) / BaseTicTacToeViewController.rowSize == index / BaseTicTacToeViewController.rowSize) {
            neighboursIndex.append(index - 1)
        }
        if ((index + 1) / BaseTicTacToeViewController.rowSize == index / BaseTicTacToeViewController.rowSize) {
            neighboursIndex.append(index + 1)
        }
        
        if (neighboursIndex.contains(indexPathItem) && items[indexPathItem].value == .E) {
            return true
        }
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        if (items.filter { $0.value == playerSymbol }.count == 3) {
            if (selectedItemIndex == nil) {
                if (items[index].value == playerSymbol) {
                    selectedItemIndex = index
                    collectionView.reloadData()
                }
            }
            else if (selectedItemIndex != index) {
                if (items[index].value == .E && isFreeAdjacent(indexPath.item)) {
                    cellClickedAction(index)
                }
            }
            else {
                // Unselect the selected index
                selectedItemIndex = nil
                collectionView.reloadData()
            }
        }
        else if (items[index].value == .E) {
            cellClickedAction(index)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / CGFloat(BaseTicTacToeViewController.rowSize)
        let height = width
        
        return CGSize(width: width, height: height)
    }
}
