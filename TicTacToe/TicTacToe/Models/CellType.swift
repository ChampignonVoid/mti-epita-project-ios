//
//  CellType.swift
//  TicTacToe
//
//  Created by Kiri on 28/11/2019.
//  Copyright © 2019 Kiri. All rights reserved.
//

import UIKit

enum CellType: String, Codable {
    case X = "X"
    case O = "O"
    case E = " " // empty
}
