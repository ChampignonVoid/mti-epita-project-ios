//
//  MultipeerTicTacToeData.swift
//  TicTacToe
//
//  Created by Kiri on 24/11/2019.
//  Copyright © 2019 Kiri. All rights reserved.
//

import UIKit

struct MultipeerTicTacToeData: Codable {
    let oldIndex: Int
    let newIndex: Int
    let symbol: CellType
    
    init(oldIndex: Int, newIndex: Int, symbol: CellType) {
        self.oldIndex = oldIndex
        self.newIndex = newIndex
        self.symbol = symbol
    }
}
